package com.dev.corsapi;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("clientes")
@CrossOrigin
public class Controller {
    private List<Cliente> clientes = new ArrayList<>();

    public Controller() {
        this.clientes.add(new Cliente(UUID.randomUUID(), "Maria", "Leão"));
        this.clientes.add(new Cliente(UUID.randomUUID(), "Leonardo", "Campos"));
        this.clientes.add(new Cliente(UUID.randomUUID(), "Edmundo", "Roberto"));
    }

    private HttpHeaders addCorsHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "OPTIONS");
        headers.add("Access-Control-Allow-Headers", "*");
        headers.add("Access-Control-Max-Age", "1800");
        return headers;
    }

    @GetMapping
    public ResponseEntity<List<Cliente>> buscarTodos() {
        return ResponseEntity.ok()
                /*.headers(addCorsHeaders())*/
                .body(clientes);
    }

    @PostMapping
    public ResponseEntity<Void> cadastrar(@RequestBody Cliente cliente) {
        // geração do UUID do novo cliente
        cliente.setId(UUID.randomUUID());
        clientes.add(cliente);
        return new ResponseEntity<Void>
                (/*addCorsHeaders(),*/ HttpStatus.CREATED);
    }

    @PutMapping("{clienteId}")
    public ResponseEntity<Void> editar(@RequestBody Cliente cliente) {
        var clienteAEditar = clientes
                .stream()
                .filter(c -> c.getId().equals(cliente.getId()))
                .findFirst();
        if(clienteAEditar.isEmpty()) {
            return new ResponseEntity<Void>
                    (HttpStatus.NOT_FOUND);
        }else {
            clienteAEditar.get().setNome(cliente.getNome());
            clienteAEditar.get().setSobreNome(cliente.getSobreNome());
            return new ResponseEntity<Void>
                    (HttpStatus.OK);
        }


    }

    @DeleteMapping("{clienteId}")
    public ResponseEntity<Void> excluir(@PathVariable UUID clienteId) {
        // remoção do cliente pelo UUID
        clientes.removeIf(cliente -> cliente.getId().equals(clienteId));

        return new ResponseEntity<Void>
                (/*addCorsHeaders(),*/ HttpStatus.NO_CONTENT);
    }




}
